let autocomplete
function initAutocomplete() {
    autocomplete = new google.maps.places.Autocomplete(
        document.getElementById('autocomplete'),
        {
            types: ['address'],
            componentRestrictions: { 'country': ['au'] },
            fields: ['place_id', 'formatted_address']
        })
    autocomplete.addListener('place_changed', onPlaceChanged)
}
function onPlaceChanged() {
    console.log("place changed")
    // var place = autocomplete.getPlace()
    // autocomplete.innerText = place.formatted_address
    // if (!place.geometry) {
    //     // User did not select a prediction; reset the input field
    //     document.getElementById('autocomplete'.placeholder = 'Enter a place')
    //     console.log(place)

    // } else {
    //     // Display details about the valid place
    //     document.getElementById('details').innerHTML = place.name
    // }
}