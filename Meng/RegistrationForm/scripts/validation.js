// const fname = document.getElementById('name')
// const address = document.getElementById('address')
const form = document.querySelector('form')
const email = document.getElementById('email')
const emailAlert = document.getElementById('email-alert')
const password = document.getElementById('password')
const repassword = document.getElementById('repassword')
const phone = document.getElementById('phone')
const phoneAlert = document.getElementById('phone-alert')
const bday = document.getElementById('bday')
const agedCare = document.getElementById('aged-care')
const underageAlert = document.getElementById('underage-alert')

let isValid = false

// email validation listener
email.addEventListener('input', (e) => {
    if (!/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/.test(e.target.value)) {
        emailAlert.hidden = false
        email.style.border = "1px solid red"
        isValid = false
    }
    else {
        emailAlert.hidden = true
        email.style.border = "1px solid green"
        isValid = true
    }
})

// phone validation listener
phone.addEventListener('input', (e) => {
    if (!/^\+(\d[\d-. ]+)?(\([\d-. ]+\))?[\d-. ]+\d$/.test(e.target.value)) {
        phoneAlert.hidden = false
        phone.style.border = "1px solid red"
        isValid = false
    }
    else {
        phoneAlert.hidden = true
        phone.style.border = "1px solid green"
        isValid = true
    }
})

// age listener
bday.addEventListener('input', (e) => {
    var age = calcAge(e.target.valueAsNumber)
    console.log(age)
    if (age > 50) {
        underageAlert.hidden = true
        agedCare.hidden = false
        bday.style.border = "1px solid blue"
        isValid = true
    }
    else if (age < 13) {
        underageAlert.hidden = false
        agedCare.hidden = true
        bday.style.border = "1px solid red"
        isValid = false
    }
})

function calcAge(birthday) {
    var ageGap = Date.now() - birthday
    var ageDate = new Date(ageGap)
    return Math.abs(ageDate.getUTCFullYear() - 1970)
}

// submission listener
form.addEventListener('submit', (e) => {
    console.log(isValid)
    if (!isValid) { e.preventDefault() }
})

