$(document).ready(function(){
    var boxHeight = $(".card").height();
    $(".card").animate({
        height: "450"
    });
    $(".card_body").hide();
    $(".card").mouseenter(function(){
        $(this).animate({
            height: "100%"
        });
        $(".card_body").show();

    }).mouseleave(function(){
        $(this).animate({
            height: "450"
        });
        $(".card_body").hide();
    });
});