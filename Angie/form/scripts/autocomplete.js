const saddress = document.getElementById('saddress');
const matchlist = document.getElementById('match-list');

function genApi(address) {
    return `https://api.addressify.com.au/addresspro/autocomplete?api_key=22e9c4ff-dce0-4caf-bd9a-b33d8d76c1b6&term=${address.value}`;
 };
//search list
const searchAddress = async searchText => {
    const url = genApi(saddress);
    const res = await fetch(url);
    const address = await res.json();

    if (searchText.length ===0) {
        matchlist.innerHTML = '';
    };
    outputHtml(address);
    
};

function setAddress(addressId) {
    document.getElementById("saddress").value = addressId.innerHTML;
    matchlist.innerHTML = '';
};

//Show results in HTML
const outputHtml = address => {
    if(address.length >0 ){
        const html = address.map(address => `
        <div class="card listHover">
        <p onclick="setAddress(this)" class="listHover">${address}</p>        
        </div>
        `).join('');
        matchlist.innerHTML = html;
    }
};

saddress.addEventListener('input', ()=> searchAddress(saddress.value));