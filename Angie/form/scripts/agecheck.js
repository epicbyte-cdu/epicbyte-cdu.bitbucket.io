
function checkAge() {
  var dob = new Date(document.getElementById("birthdate").value);
  var current = new Date();
  var maxYear = current.getFullYear()-13;
  var minYear = current.getFullYear()-50;
  var thirteen = new Date(maxYear,current.getMonth(), current.getDate());
  var fifty = new Date(minYear,current.getMonth(), current.getDate());
  if (dob>thirteen){
    young = "You need a parent or guardian to register on your behalf for our conference. Otherwsie, please come back when you are 13.";
    old = "";
    birthdate.setCustomValidity("Invalid field.");
  }
  else if (dob<fifty) {
    old = "Need some help registering? We can help you";
    young = "";
    birthdate.setCustomValidity("");
  }
  else {
    old = "";
    young = "";
    birthdate.setCustomValidity("");
  }
  document.getElementById("ageyoung").innerHTML = young;
  document.getElementById("ageold").innerHTML = old;
};

document.getElementById("birthdate").addEventListener('input', ()=> checkAge());

