var slides = document.getElementsByClassName("Slides");
var dots = document.getElementsByClassName("dot");

var slideIndex = 0;
showSlides();

function showSlides() {
  // carousel basic behaviour
  var i;
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
  }
  slideIndex++;
  if (slideIndex > slides.length) { slideIndex = 1 }//reset
  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" active", "");//change classname from "dot active" to "dot"
  }
  slides[slideIndex - 1].style.display = "block";
  dots[slideIndex - 1].className += " active";
  setTimeout(showSlides, 5000); // Change image every 5 seconds
}

function currentSlide(e) {
  // dispaly current slide while hide others

  var slidesArray = [...slides]
  var dotsArray = [...dots]
  var current = e - 1//current index of element

  slidesArray.splice(current, 1)
  dotsArray.splice(current, 1)

  slides[current].style.display = "block"
  dots[current].className += " active"

  slidesArray.map(slide => slide.style.display = "none")
  dotsArray.map(dot => dot.className = dot.className.replace(" active", ""))

}